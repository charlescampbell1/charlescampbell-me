# frozen_string_literal: true

module AuthenticationHelper
  def http_login
    request.env['HTTP_AUTHORIZATION'] =
      ActionController::HttpAuthentication::Basic.encode_credentials(
        'ccampbell', 'password'
      )
  end
end
