# frozen_string_literal: true

FactoryBot.define do
  factory :highlight do
    title { 'Hightlight' }
    organisation { 'The World' }
    start_date { Time.at(1_586_865_600) }
    end_date { Time.at(1_589_457_600) }
    description { 'This is a description' }
  end
end
