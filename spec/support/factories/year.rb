# frozen_string_literal: true

FactoryBot.define do
  factory :year do
    title { 'First Year' }
    grade { 'FIRST' }
  end
end
