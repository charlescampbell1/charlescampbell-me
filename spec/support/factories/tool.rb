# frozen_string_literal: true

FactoryBot.define do
  factory :tool do
    tool { 'ruby' }
    colour { 'red' }
  end
end
