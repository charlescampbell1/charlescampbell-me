# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    tag { 'ruby' }
    colour { 'red' }
  end
end
