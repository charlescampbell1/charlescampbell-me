# frozen_string_literal: true

FactoryBot.define do
  factory :unit do
    unit { 'The Art of Testing' }
    grade { 'FIRST' }
    description { 'RSpec and friends' }
  end
end
