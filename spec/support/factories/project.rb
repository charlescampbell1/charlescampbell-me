# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    friendly_name { 'charlescampbell.me' }
    url { 'http://www.charlescampbell.me' }
    position { '1' }
  end
end
