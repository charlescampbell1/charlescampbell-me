# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TagController, type: :controller do
  before { http_login }

  let(:unit) { create(:unit) }

  describe '#new' do
    it 'assigns a new tag to @tag' do
      get :new, params: { unit: 1 }

      expect(assigns(:tag)).to be_a_new(Tag)
    end
  end

  describe '#create' do
    context 'with valid attributes' do
      it 'creates a new tag' do
        qualification = create(:qualification)
        year = create(:year)
        unit = create(:unit)

        qualification.years << year
        year.units << unit

        expect { post :create, params: { tag: FactoryBot.attributes_for(:tag), unit: unit } }
          .to change(Tag, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new tag' do
        qualification = create(:qualification)
        year = create(:year)
        unit = create(:unit)

        qualification.years << year
        year.units << unit

        expect { post :create, params: { tag: FactoryBot.attributes_for(:tag, colour: nil), unit: unit } }
          .not_to change(Tag, :count)
      end
    end
  end

  describe '#destroy' do
    it 'deletes the tag' do
      qualification = create(:qualification)
      year = create(:year)
      unit = create(:unit)
      tag = create(:tag)

      qualification.years << year
      year.units << unit
      unit.tags << tag

      expect { delete :destroy, params: { id: tag.id } }.to change(Tag, :count).by(-1)
    end
  end
end
