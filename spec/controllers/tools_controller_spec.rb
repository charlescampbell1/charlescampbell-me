# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ToolsController, type: :controller do
  before { http_login }

  describe '#new' do
    it 'assigns a new tool to @tool' do
      get :new
      expect(assigns(:tool)).to be_a_new(Tool)
    end
  end

  describe '#create' do
    context 'with valid attributes' do
      it 'creates a new tool' do
        expect { post :create, params: { tool: FactoryBot.attributes_for(:tool) } }.to change(Tool, :count).by(1)
      end
    end

    context 'with incorrect attributes' do
      it 'does not create a new tool' do
        expect do
          post :create, params: {
            tool: FactoryBot.attributes_for(:tool, colour: nil)
          }
        end.not_to change(Tool, :count)
      end
    end
  end

  describe '#destroy' do
    it 'deletes the tool' do
      tool = create(:tool)

      expect { delete :destroy, params: { id: tool.id } }.to change(Tool, :count).by(-1)
    end
  end
end
