# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProjectController, type: :controller do
  before { http_login }

  describe '#index' do
    before do
      get :index
    end

    it 'renders the frontend index template' do
      expect(response).to render_template('index')
    end

    it 'sets @tools to be a list of tools' do
      project = create(:project)

      expect(assigns(:projects)).to eq([project])
    end
  end

  describe '#backend' do
    context 'with successful authentication' do
      before do
        http_login
        get :backend
      end

      it 'returns a 200 success response' do
        expect(response).to have_http_status(:ok)
      end

      it 'renders the backend index template' do
        expect(response).to render_template('index')
      end

      it 'sets @project to be a list of tools' do
        project = create(:project)

        expect(assigns(:projects)).to eq([project])
      end
    end
  end

  describe '#new' do
    it 'assigns a new project to @project' do
      get :new, format: :js, xhr: true
      expect(assigns(:project)).to be_a_new(Project)
    end
  end

  describe '#edit' do
    it 'gets the project record' do
      project = create(:project)

      get :edit, params: { id: project.id }, format: :js, xhr: true

      expect(assigns(:project)).to eq(project)
    end
  end

  describe '#create' do
    context 'with valid attributes' do
      it 'creates a new project' do
        expect do
          post :create, params: {
            project: FactoryBot.attributes_for(:project)
          }
        end.to change(Project, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new project' do
        expect do
          post :create, params: {
            project: FactoryBot.attributes_for(:project, friendly_name: nil)
          }
        end.not_to change(Project, :count)
      end
    end
  end

  describe '#update' do
    let(:new_friendly_name) { 'New friendly_name' }

    before do
      @project = create(:project)
    end

    context 'with valid attributes' do
      it 'updates the project friendly_name' do
        put :update, params: {
          id: @project.id,
          project: FactoryBot.attributes_for(:project, friendly_name: new_friendly_name)
        }

        @project.reload
        expect(@project.friendly_name).to eq(new_friendly_name)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the project friendly_name' do
        put :update, params: {
          id: @project.id,
          project: FactoryBot.attributes_for(:project, friendly_name: nil)
        }

        @project.reload
        expect(@project.friendly_name).to eq(@project.friendly_name)
      end
    end
  end

  describe '#destroy' do
    it 'deletes the project' do
      project = create(:project)

      expect { delete :destroy, params: { id: project.id } }
        .to change(Project, :count).by(-1)
    end
  end
end
