# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EducationController, type: :controller do
  before { http_login }

  describe '#index' do
    before { get :index }

    it 'renders the frontend index template' do
      expect(response).to render_template('index')
    end

    it 'sets @qualifications to be a list of qualification' do
      qualification = create(:qualification)

      expect(assigns(:qualifications)).to eq([qualification])
    end
  end

  describe '#backend' do
    context 'with successful authentication' do
      before do
        http_login
        get :backend
      end

      it 'returns a 200 success response' do
        expect(response).to have_http_status(:ok)
      end

      it 'renders the backend index template' do
        expect(response).to render_template('index')
      end

      it 'sets @qualifications to be a list of qualification' do
        qualification = create(:qualification)

        expect(assigns(:qualifications)).to eq([qualification])
      end
    end
  end

  describe '#qualification_details' do
    before do
      @qualification = create(:qualification)
      http_login
      get :qualification_details, params: { id: @qualification.id }
    end

    it 'renders the details template' do
      expect(response).to render_template('details')
    end

    it 'sets the correct qualification' do
      expect(assigns(:qualification)).to eq(@qualification)
    end
  end

  describe '#new_qualification' do
    it 'assigns a new qualification instance' do
      get :new_qualification

      expect(assigns(:qualification)).to be_a_new(Qualification)
    end
  end

  describe '#create_qualification' do
    context 'with valid attributes' do
      it 'creates a new qualification' do
        expect do
          post :create_qualification, params: { qualification: FactoryBot.attributes_for(:qualification) }
        end.to change(Qualification, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create the qualification' do
        expect do
          post :create_qualification, params: { qualification: FactoryBot.attributes_for(:qualification, title: nil) }
        end.not_to change(Qualification, :count)
      end
    end
  end

  describe '#edit_qualification' do
    it 'gets the qualifcation record' do
      qualification = create(:qualification)

      get :edit_qualification, params: { id: qualification.id }

      expect(assigns(:qualification)).to eq(qualification)
    end
  end

  describe '#update_qualification' do
    let(:new_title) { 'New Title' }

    before { @qualification = create(:qualification) }

    context 'with valid attributes' do
      it 'updates the qualification title' do
        put :update_qualification, params: {
          id: @qualification.id,
          qualification: FactoryBot.attributes_for(:qualification, title: new_title)
        }

        @qualification.reload

        expect(@qualification.title).to eq(new_title)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the qualification title' do
        put :update_qualification, params: {
          id: @qualification.id,
          qualification: FactoryBot.attributes_for(:qualification, title: nil)
        }

        @qualification.reload
        expect(@qualification.title).to eq(@qualification.title)
      end
    end
  end

  describe '#destroy_qualification' do
    it 'deletes the qualification' do
      qualification = create(:qualification)

      expect { delete :destroy_qualification, params: { id: qualification.id } }.to change(Qualification, :count).by(-1)
    end
  end

  describe '#new_year' do
    it 'assigns a new year instance' do
      get :new_year, params: { qualification: 1 }

      expect(assigns(:year)).to be_a_new(Year)
    end
  end

  describe '#create_year' do
    context 'with valid attributes' do
      it 'creates a new year' do
        qualification = create(:qualification)

        expect do
          post :create_year, params: { year: FactoryBot.attributes_for(:year), qualification: qualification }
        end.to change(Year, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create the year' do
        qualification = create(:qualification)

        expect do
          post :create_year, params: {
            year: FactoryBot.attributes_for(:year, title: nil),
            qualification: qualification
          }
        end.not_to change(Year, :count)
      end
    end
  end

  describe '#edit_year' do
    it 'gets the year record' do
      year = create(:year)

      get :edit_year, params: { id: year.id }

      expect(assigns(:year)).to eq(year)
    end
  end

  describe '#update_year' do
    let(:new_title) { 'New Title' }

    before do
      @year = create(:year)
      @qualification = create(:qualification)

      @qualification.years << @year
    end

    context 'with valid attributes' do
      it 'updates the year title' do
        put :update_year, params: { id: @year.id, year: FactoryBot.attributes_for(:year, title: new_title) }

        @year.reload

        expect(@year.title).to eq(new_title)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the year title' do
        put :update_year, params: { id: @year.id, year: FactoryBot.attributes_for(:year, title: nil) }

        @year.reload

        expect(@year.title).to eq(@year.title)
      end
    end
  end

  describe '#destroy_year' do
    it 'deletes the year' do
      year = create(:year)
      qualification = create(:qualification)

      qualification.years << year

      expect { delete :destroy_year, params: { id: year.id } }.to change(Year, :count).by(-1)
    end
  end

  describe '#new_unit' do
    it 'assigns a new unit instance' do
      get :new_unit, params: { year: 1 }

      expect(assigns(:unit)).to be_a_new(Unit)
    end
  end

  describe '#create_unit' do
    context 'with valid attributes' do
      it 'creates a new unit' do
        qualification = create(:qualification)
        year = create(:year)

        qualification.years << year

        expect { post :create_unit, params: { unit: FactoryBot.attributes_for(:unit), year: year } }
          .to change(Unit, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create the year' do
        qualification = create(:qualification)
        year = create(:year)

        qualification.years << year

        expect { post :create_unit, params: { unit: FactoryBot.attributes_for(:unit, unit: nil), year: year } }
          .not_to change(Year, :count)
      end
    end
  end

  describe '#edit_unit' do
    it 'gets the unit record' do
      unit = create(:unit)

      get :edit_unit, params: { id: unit.id }

      expect(assigns(:unit)).to eq(unit)
    end
  end

  describe '#update_unit' do
    let(:new_unit) { 'New Unit' }

    before do
      @qualification = create(:qualification)
      @year = create(:year)
      @unit = create(:unit)

      @qualification.years << @year
      @year.units << @unit
    end

    context 'with valid attributes' do
      it 'updates the unit' do
        put :update_unit, params: { id: @unit.id, unit: FactoryBot.attributes_for(:unit, unit: new_unit) }

        @unit.reload

        expect(@unit.unit).to eq(new_unit)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the unit' do
        put :update_unit, params: { id: @unit.id, unit: FactoryBot.attributes_for(:unit, unit: nil) }

        @unit.reload

        expect(@unit.unit).to eq(@unit.unit)
      end
    end
  end

  describe '#destroy_unit' do
    it 'deletes the unit' do
      qualification = create(:qualification)
      year = create(:year)
      unit = create(:unit)

      qualification.years << year
      year.units << unit

      expect { delete :destroy_unit, params: { id: unit.id } }.to change(Unit, :count).by(-1)
    end
  end
end
