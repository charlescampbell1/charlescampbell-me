# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HighlightController, type: :controller do
  before { http_login }

  describe '#index' do
    it 'renders the index page' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe '#backend' do
    it 'renders the backend page' do
      get :backend
      expect(response).to render_template('backend')
    end
  end

  describe '#new' do
    it 'assigns a new highlight to @highlight' do
      get :new, format: :js, xhr: true
      expect(assigns(:highlight)).to be_a_new(Highlight)
    end
  end

  describe '#edit' do
    it 'gets the highlight record' do
      highlight = create(:highlight)

      get :edit, params: { id: highlight.id }

      expect(assigns(:highlight)).to eq(highlight)
    end
  end

  describe '#create' do
    context 'with valid attributes' do
      it 'creates a new highlight' do
        expect do
          post :create, params: {
            highlight: FactoryBot.attributes_for(:highlight)
          }
        end.to change(Highlight, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new highlight' do
        expect do
          post :create, params: {
            highlight: FactoryBot.attributes_for(:highlight, title: nil)
          }
        end.not_to change(Highlight, :count)
      end
    end
  end

  describe '#update' do
    let(:new_title) { 'New Title' }

    before do
      @highlight = create(:highlight)
    end

    context 'with valid attributes' do
      it 'updates the highlight title' do
        put :update, params: {
          id: @highlight.id,
          highlight: FactoryBot.attributes_for(:highlight, title: new_title)
        }

        @highlight.reload
        expect(@highlight.title).to eq(new_title)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the highlight title' do
        put :update, params: {
          id: @highlight.id,
          highlight: FactoryBot.attributes_for(:highlight, title: nil)
        }

        @highlight.reload
        expect(@highlight.title).to eq(@highlight.title)
      end
    end
  end

  describe '#destroy' do
    it 'deletes the highlight' do
      highlight = create(:highlight)

      expect { delete :destroy, params: { id: highlight.id } }
        .to change(Highlight, :count).by(-1)
    end
  end
end
