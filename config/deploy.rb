# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock '~> 3.16.0'

set :application, 'charlescampbell'
set :repo_url, 'git@gitlab.com:charlescampbell1/charlescampbell-me.git'
set :deploy_to, "/home/charlescampbell/#{fetch :application}"
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
       'vendor/bundle', '.bundle', 'public/system', 'public/uploads'

set :keep_releases, 2

set :passenger_restart_with_touch, true
