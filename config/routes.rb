# frozen_string_literal: true

Rails.application.routes.draw do
  root 'highlight#index'

  get 'home/index', to: 'highlight#index', as: :home
  get 'projects', to: 'project#index', as: :project
  get 'education', to: 'education#index', as: :education
  get 'history', to: 'gallary#history', as: :history_gallary
  get 'backend', to: 'highlight#backend', as: :backend_root

  scope :backend do
    get 'home', to: 'highlight#backend', as: :backend
    get 'projects', to: 'project#backend', as: :backend_projects
    get 'education', to: 'education#backend', as: :backend_education
    get 'gallary', to: 'gallary#backend', as: :backend_gallary
    get 'tools', to: 'tools#backend', as: :backend_tools

    scope :admin do
      scope :profile do
        get 'new', to: 'admin#new_profile', as: :new_profile
        get 'edit/:id', to: 'admin#edit_profile', as: :edit_profile
        post 'create', to: 'admin#create_profile', as: :create_profile
        patch 'update/:id', to: 'admin#update_profile', as: :update_profile
      end

      scope :social do
        get 'new', to: 'admin#new_social', as: :new_social
        get 'edit/:id', to: 'admin#edit_social', as: :edit_social
        post 'create', to: 'admin#create_social', as: :create_social
        patch 'update/:id', to: 'admin#update_social', as: :update_social
        delete 'delete/:id', to: 'admin#destroy_social', as: :delete_social
      end
    end
  end

  scope :highlight do
    get 'new', to: 'highlight#new', as: :new_highlight
    get 'edit/:id', to: 'highlight#edit', as: :edit_highlight
    post 'create', to: 'highlight#create', as: :create_highlight
    patch 'update/:id', to: 'highlight#update', as: :update_highlight
    delete 'delete/:id', to: 'highlight#destroy', as: :delete_highlight
  end

  scope :project do
    get 'new', to: 'project#new', as: :new_project
    get 'edit/:id', to: 'project#edit', as: :edit_project
    post 'create', to: 'project#create', as: :create_project
    patch 'update/:id', to: 'project#update', as: :update_project
    delete 'delete/:id', to: 'project#destroy', as: :delete_project
  end

  scope :education do
    scope :qualification do
      get 'details/:id', to: 'education#qualification_details', as: :qualification_details
      get 'new', to: 'education#new_qualification', as: :new_qualification
      post 'create', to: 'education#create_qualification', as: :create_qualification
      get 'edit/:id', to: 'education#edit_qualification', as: :edit_qualification
      patch 'update/:id', to: 'education#update_qualification', as: :update_qualification
      delete 'delete/:id', to: 'education#destroy_qualification', as: :delete_qualification
    end

    scope :year do
      get 'new/:qualification', to: 'education#new_year', as: :new_year
      get 'edit/:id', to: 'education#edit_year', as: :edit_year
      post 'create/:qualification', to: 'education#create_year', as: :create_year
      patch 'update/:id', to: 'education#update_year', as: :update_year
      delete 'delete/:id', to: 'education#destroy_year', as: :delete_year
    end

    scope :unit do
      get 'new/:year', to: 'education#new_unit', as: :new_unit
      get 'edit/:id', to: 'education#edit_unit', as: :edit_unit
      post 'create/:year', to: 'education#create_unit', as: :create_unit
      patch 'update/:id', to: 'education#update_unit', as: :update_unit
      delete 'delete/:id', to: 'education#destroy_unit', as: :delete_unit
    end

    scope :tag do
      get 'new/:unit', to: 'tag#new', as: :new_tag
      post 'create/:unit', to: 'tag#create', as: :create_tag
      delete 'delete/:id', to: 'tag#destroy', as: :delete_tag
    end
  end

  scope :tools do
    get 'new', to: 'tools#new', as: :new_tool
    post 'create', to: 'tools#create', as: :create_tool
    delete 'delete/:id', to: 'tools#destroy', as: :delete_tool
  end
end
