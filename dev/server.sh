#!/bin/bash

docker-compose up

docker-compose run app bundle exec rake db:migrate

docker-compose run app bundle exec rake db:seed