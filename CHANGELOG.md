# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.0.3](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v3.0.2...v3.0.3) (2021-11-13)


### Bug Fixes

* **highlight:** Any date in the future marks highlight as present ([2c5e9a3](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/2c5e9a30e1783c73cc319c77181d3180f2617e23))

### [3.0.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v3.0.1...v3.0.2) (2021-08-25)

### [3.0.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v3.0.0...v3.0.1) (2021-08-23)


### Bug Fixes

* **backend:** Fix admin_path route ([c46207d](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/c46207d1f81a15cecf9b4264e32203394702f515))
* **style:** Fixed issue where mobile users couldn't see content hidden by footer ([23bdbd7](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/23bdbd7ccf16193f9d08551f852ae4e832c98daf))

## [3.0.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.5.0...v3.0.0) (2021-08-22)


### ⚠ BREAKING CHANGES

* **ruby:** Bump ruby to latest version 3.0.2

### Features

* **analytics:** Remove ahoy analytics ([719c971](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/719c971a0edd859fab4143e0f696030df68a1627))
* **ruby:** Bump ruby to latest version 3.0.2 ([ebe66f4](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/ebe66f4ebf399c5544a79bef6279527fef6d169e))

## [2.5.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.4.0...v2.5.0) (2021-08-21)


### Features

* **blog:** Add blog posts to projects ([7f7a3d4](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/7f7a3d4b03e001599338743f0f6f4755ee70b1cb))
* **project:** Update project forms ([6ee1438](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/6ee1438d8a40a8f584f10876a1d0ce54a4d098f3))

## [2.4.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.3.1...v2.4.0) (2021-08-21)


### Features

* **highlight:** Improve highlight form ([94c99cf](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/94c99cf156259b62a74be6afe6a3afd3b871a7e7))

### [2.3.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.3.0...v2.3.1) (2021-08-20)

## [2.3.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.2.3...v2.3.0) (2021-08-20)


### Features

* **badge:** Replace badges with a cleaner text based tag ([51ba1f6](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/51ba1f6a3bfa09adae6446f454e52273f380e75c))

### [2.2.3](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.2.2...v2.2.3) (2021-08-19)


### Bug Fixes

* **deploy:** Fix syntax error preventing deployment ([807ba1b](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/807ba1bd53da9b9e23a0827a8a93e2147feed073))

### [2.2.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.2.1...v2.2.2) (2021-08-19)


### Bug Fixes

* **deploy:** Update uglifier to specify harmony true ([219015f](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/219015f91b37dd34c027da02be6b9effd8f35452))

### [2.2.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.2.0...v2.2.1) (2021-08-19)

## [2.2.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.1.3...v2.2.0) (2021-08-19)


### Features

* **blog:** Remove blog in favour of medium ([4064c81](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/4064c811487caa1052c30e658590ce7aad6f51f2))
* **highlight:** Sort by date descending ([df7a550](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/df7a550816ddac90f7cb95f7920d35b0ed43a786))

### [2.1.3](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.1.2...v2.1.3) (2021-08-19)

### [2.1.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.1.1...v2.1.2) (2020-11-05)


### Bug Fixes

* **footer:** Remove white space from bottom of web page ([878b8f2](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/878b8f2c36366f642bbd6d407c723fc993a85471))

### [2.1.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.1.0...v2.1.1) (2020-11-05)

## [2.1.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.0.1...v2.1.0) (2020-11-05)


### Features

* **gallery:** Remove gallery ([1fa49ce](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/1fa49ce7880f7e2143870cc0da62073e6cc20ec6))

### [2.0.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v2.0.0...v2.0.1) (2020-11-05)


### Bug Fixes

* **tag:** Autofocus form on tags ([94fbd42](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/94fbd428c4f4e6b757505eb3b8417aafebe4d6f2))

## [2.0.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.22.3...v2.0.0) (2020-11-05)


### ⚠ BREAKING CHANGES

* **tag:** This commit breaks the way that tags used to work with the old layout for education
* **education:** Swapped out the use of academic years and education for years and units

* **education:** Replace some of the older education code ([2a01c7f](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/2a01c7f7e4724ebc64de47439cf6b2e1f79ebe4f))
* **tag:** Update tags to work with the new education layout ([49a69c4](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/49a69c48b73c5c84dbb74bd38e23b45c283750fe))

### [1.22.3](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.22.2...v1.22.3) (2020-10-30)


### Bug Fixes

* **mobile:** Better spacing between mobile links ([9a9b5b9](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/9a9b5b98967fe4455d51ae2d2f10b556bd7dfc8f))

### [1.22.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.22.1...v1.22.2) (2020-10-27)


### Bug Fixes

* **blog:** Allow paragraphs in blog posts ([fd391f0](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/fd391f0f7660d8f4fb9d5f326af675be98e40647))

### [1.22.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.22.0...v1.22.1) (2020-10-27)


### Bug Fixes

* **blog:** Blog page will now load ([39c8ba6](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/39c8ba6062ffe55e8f7e1daee3557e0d186285ea))

## [1.22.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.21.1...v1.22.0) (2020-10-27)


### Features

* **blog:** Add endpoint for blog posts ([654edc2](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/654edc28d2f7e0dc0bda924764a058184bf84650))

### [1.21.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.21.0...v1.21.1) (2020-10-27)


### Bug Fixes

* **badge:** Better spacing on homepage ([f2a1b3d](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/f2a1b3d6275a073ca64860e6ca4bd08d6f8c7190))

## [1.21.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.20.0...v1.21.0) (2020-10-27)


### Features

* **tools:** Move toolkit to homepage ([7cfdeb5](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/7cfdeb5a3161e5460ce1000f5eeb15ec7a3ab1ed))


### Bug Fixes

* **footer:** Prevent text from being cut off ([e41b92d](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/e41b92d45a582cae101d9389488f31184795d724))
* **responsive:** Better readability in mobile view ([ba8963d](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/ba8963dc43c41700e35b72f817dc45e2f9c9b540))

## [1.20.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.19.1...v1.20.0) (2020-09-03)


### Features

* **analytics:** Output tracked events ([ba37bc0](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/ba37bc0181c4e998d48f765470dae51ba4f5a4c2))
* **analytics:** Track page visits with labels ([c360806](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/c36080615e7815cfe6c6014cd7f1b0c03415455c))

### [1.19.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.19.0...v1.19.1) (2020-09-03)


### Bug Fixes

* maybe this works? ([e7e8408](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/e7e8408cc713b61aafc38747d3e3d5f2d2ad0451))

## [1.19.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.18.2...v1.19.0) (2020-09-03)


### Features

* **analytics:** Add charts grouping device types ([75af977](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/75af9779109ba15e4825494e713e2888914584fb))

### [1.18.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.18.1...v1.18.2) (2020-09-03)


### Bug Fixes

* **analytics:** Wrap row instead of table with loop ([1fd9bdb](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/1fd9bdb512089a0c11eac208be228940de04b53c))

### [1.18.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.18.0...v1.18.1) (2020-09-03)


### Bug Fixes

* **analytics:** Sort by latest ([0c28cfa](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/0c28cfa5cba793eeadaf71dc7465d6312388caa5))

## [1.18.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.8...v1.18.0) (2020-09-03)


### Features

* **analytics:** Add page for reading analytics ([183740f](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/183740fde4798ad022d4509682a77fc27e023b19))

### [1.17.8](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.7...v1.17.8) (2020-08-27)

### [1.17.7](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.6...v1.17.7) (2020-08-14)

### [1.17.6](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.5...v1.17.6) (2020-08-13)

### [1.17.5](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.4...v1.17.5) (2020-07-19)

### [1.17.4](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.3...v1.17.4) (2020-07-19)


### Bug Fixes

* **versioning:** Fix versioning compare url format ([076682c](https://gitlab.com/charlescampbell1/charlescampbell-me/charlescampbell.me/commit/076682c5f5d55cca8a4d588f2e68cc826ae0b857))

### [1.17.3](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.2...v1.17.3) (2020-07-19)

### [1.17.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.1...v1.17.2) (2020-07-19)


### Bug Fixes

* **tools:** Specify ruby version in capfile ([af422ae](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/af422ae6c7b344daba44b08a2adae37f1a919726))

### [1.17.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.17.0...v1.17.1) (2020-07-19)


### Bug Fixes

* **tools:** Switch to using gitlab for deployments ([d76cff2](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/d76cff2b975d8ff5c4b8cafdb548b62b9b55cd0e))

## [1.17.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.16.0...v1.17.0) (2020-07-19)


### Features

* **upgrade:** Upgrade docker compose script ([8b5fdfa](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/8b5fdfa4a34b32781399c6987c4fd1e112ba3411))

## [1.16.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.6...v1.16.0) (2020-07-08)


### Features

* **project:** Dockerize application ([f7d6b9b](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/f7d6b9b919fb965b335500e5d0b1c961c5b11043))

### [1.15.6](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.5...v1.15.6) (2020-06-25)

### [1.15.5](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.4...v1.15.5) (2020-06-25)

### [1.15.4](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.3...v1.15.4) (2020-06-25)

### [1.15.3](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.2...v1.15.3) (2020-06-25)

### [1.15.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.1...v1.15.2) (2020-06-25)


### Bug Fixes

* **seed:** Add urls ([f09e5d1](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/f09e5d1c9eac3e06fd1f77462ec4a224c598ff4c))

### [1.15.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.15.0...v1.15.1) (2020-06-25)


### Bug Fixes

* **home:** Revert better checking for whether a highlight has a link ([3314d6b](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/3314d6b6592bb06018f4b8c6bc1e95b54c7122d3))

## [1.15.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.14.2...v1.15.0) (2020-06-25)


### Features

* **style:** System based darkmode ([23e6ccd](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/23e6ccd4d1c93a625ed36b86ce6c9d82e8c43743))


### Bug Fixes

* **home:** Better checking for whether a highlight has a link ([bf7e98f](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/bf7e98fea51963220b5a8113185ba3bd177747ea))
* **tag:** Add validation to the tag model ([6c644b6](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/6c644b6bc0e7ba4a1ec31cb990ae31d35ec73408))

### [1.14.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.14.1...v1.14.2) (2020-06-23)

### [1.14.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.14.0...v1.14.1) (2020-06-23)

## [1.14.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.13.0...v1.14.0) (2020-06-23)


### Features

* **tools:** Create a page for adding tooling ([7caa30e](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/7caa30e7c81ff1b8a1106c8f7ec06018ab1fc372))


### Bug Fixes

* **colours:** Swap colour selection to a dropdown ([36dc629](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/36dc6299bd750e5c2a01d4cdaec5ad41624b8e0e))
* **tag:** Make sure you cannot delete tags from the frontend ([03c4276](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/03c4276cb4a94410cf17218f17bb6cac382b7a11))
* **tool:** Remove new button from frontend - copy paste fail ([ecd8346](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/ecd834691418e19eb8494b78ea86bcb2366407cb))

## [1.13.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.12.2...v1.13.0) (2020-06-22)


### Features

* **tag:** Remove ability to edit tags ([0c1241f](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/0c1241ffa35ed7e6ab8f494ee067c1e9e0c9ed5e))


### Bug Fixes

* **tag:** Add tag based on education ([aeb7368](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/aeb7368adcd806fe9636eb540618ebd088a8b561))
* **tag:** Call upcase on tags ([3b9c398](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/3b9c3986838e62e65099b8016b70efd113e2c0cd))

### [1.12.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.12.1...v1.12.2) (2020-06-21)


### Bug Fixes

* **tag:** Fix the fact that I've not been editing tags ([ef466e9](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/ef466e921ed3e4c33d8d2577eb7c905eacdb453a))

### [1.12.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.12.0...v1.12.1) (2020-06-21)


### Bug Fixes

* **tag:** Slap the tags on the frontend too ([c832cd4](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/c832cd4afdeb701e476d2c397ada2ba7c2a570a7))

## [1.12.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.11.1...v1.12.0) (2020-06-21)


### Features

* **tags:** Add quick and dirty way of adding tags to units ([e27c95c](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/e27c95c3e09aa17b5219183f00e50c6da66af9e1))

### [1.11.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.11.0...v1.11.1) (2020-06-19)

## [1.11.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.10.1...v1.11.0) (2020-06-19)


### Features

* **projects:** Add support for repositioning projects ([3e2369c](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/3e2369c6d4e8d0d0c9158dd3c1e4c8eba4643f16))

### [1.10.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.10.0...v1.10.1) (2020-05-27)


### Bug Fixes

* **mobile:** Increase padding on footer ([817b02b](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/817b02bf65d18e76612cbe3ad6d51187cdd7aebc))

## [1.10.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.9.1...v1.10.0) (2020-05-27)


### Features

* **social icons:** Enable display options for mobile ([896d665](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/896d665c73b9d544c0f00f05a2be1fc32c719207))


### Bug Fixes

* **intro:** Hacky temporary fix to remove random line appearing on mobile ([a38925f](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/a38925fbb482a8e76fb0c51066009d13f00455d9))
* **intro:** Improve spacing for mobile content ([75dd9b5](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/75dd9b5a10d9a061560b4d5fede3c34abb52c21a))

### [1.9.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.9.0...v1.9.1) (2020-05-27)


### Bug Fixes

* **stylesheet:** Simplified mobile view with improvements ([7905443](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/7905443f826430f3ae0789bead6bffbd92167f5f))

## [1.9.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.8.0...v1.9.0) (2020-05-26)


### Features

* **metrics:** Add prometheus ([f48d931](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/f48d931fa1142efb2526c43395d9e5a69fef0313))

## [1.8.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.7.0...v1.8.0) (2020-05-26)


### Features

* **profile:** Add ability to change name and position ([719546a](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/719546a8b368eb4e05e12c44c0b2bd5eeebb7047))

## [1.7.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.6.0...v1.7.0) (2020-05-26)


### Features

* **backend:** Display version number ([37633a8](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/37633a8ad904ed6bab49f558b154d5da8885e108))
* **favicon:** Add dev and frontend favicons ([b7d73e1](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/b7d73e1d0476e677bd5f793f8d945806c0ae2794))

## [1.6.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.5.2...v1.6.0) (2020-05-26)


### Features

* **backend:** Hide all backend services behind basic auth ([21674d7](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/21674d70d707de0b9bf65857fb99ac5c07241320))


### Bug Fixes

* **highlights:** Sort by date descending ([16bf5ef](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/16bf5effbd10a7583a638f61c00f67418dea1eb8))

### [1.5.2](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.5.1....v1.5.2) (2020-03-22)


### Bug Fixes

* **gallery:** Stop prod options on show ([ffcad90](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/ffcad90bf91409e8d913fbbb9b7f93d8e1c81307))

### [1.5.1](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.5.0....v1.5.1) (2020-03-22)


### Bug Fixes

* **gallaries:** Hide prod menu and fix ordering ([041a650](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/041a650990e2a0b12173a089fd1b85ec2cd6d12f))

## [1.5.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.4.0....v1.5.0) (2020-03-22)


### Features

* **gallery:** Allow a gallery for uploading outdated websites ([67f966c](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/67f966c2293f59d45363f7aafaeec59777750e11))

## [1.4.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.3.0....v1.4.0) (2020-03-21)


### Features

* **academic year:** CRUD for mid level academic year ([b9cf16a](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/b9cf16a4da133beece08901f45e7e4a45b925701))
* **education:** CRUD for top level education ([56d7679](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/56d7679889f88134480a0a577526397a4a5b6b43))
* **projects:** Basic CRUD for projects page ([2b0624f](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/2b0624f466de2a816e645fd04628016c58efa9d7))
* **projects:** Link projects backend ([dd993ef](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/dd993ef2df711a4d9cf9cc180a7671c7605c3cc1))
* **qualification:** CRUD for top level qualifications ([6530d9c](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/6530d9c0dbb2ee1824754cbad483c6eb30b4b4b0))

## [1.3.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.2.0....v1.3.0) (2020-03-20)


### Features

* **backend:** Basic backend design for the homepage ([80a458c](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/80a458c23d24316f37ff2ca319a1ba76ff695ae8))
* **highlights:** Basic CRUD operators with modals ([8f695fe](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/8f695fe0d0400af102cec3d58ed09fc205d37277))
* **social links:** Basic CRUD operators with modals ([5753b51](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/5753b514009b00fb28e915f232751950283dd428))

## [1.2.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.1.0....v1.2.0) (2020-03-19)


### Features

* **education:** List educations ([4582192](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/458219240c24aa15fc976b84e213a55439965cfc))


### Bug Fixes

* **design:** Fix various design layouts ([29b58e3](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/29b58e315b6e2d600edaaea02e85cb8ae9e4c635))
* **footer:** Make footer sticky ([ab30a94](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/ab30a94d3626ec9655f3b1a0eb9cfb4109e29c66))

## [1.1.0](https://gitlab.com/charlescampbell1/charlescampbell-me/-/compare/v1.0.0....v1.1.0) (2020-03-19)


### Features

* **footer:** Active links in footer ([4dac291](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/4dac291ac7eddaf2725e523d9a06224f8b536b36))
* **footer:** Implement static footer links ([d509fa6](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/d509fa6a35136fb8be9c72ba7cc530785a599209))
* **project:** Create a list of live projects I'm working on ([f6291a7](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/f6291a71046257085d32ec260d422b8fd70e0a6b))

## 1.0.0 (2020-03-19)


### Features

* **experience:** Segment of page for education and employment ([eed61e1](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/eed61e189d5a42a0b9460e3362749bc6cb888bb1))
* **profile:** Create profile card for top of page ([bf7f29f](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/bf7f29f5d5e3ddc1a20a176645adf1c186c57e52))


### Bug Fixes

* **deployment:** Fix rake setup ([e6d8677](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/e6d8677afa91bed1cc7e332b30afe085383490e3))
* **profile:** Remove profile picture ([25f34e4](https://gitlab.com/charlescampbell1/charlescampbell-me/commit/25f34e4fa5342826f9368d7744df976c4c1ddf79))
