module.exports = config = {};

config.bumpFiles = [
  {
    filename: "VERSION",
    type: "plain-text"
  }
];

const host = "https://gitlab.com/charlescampbell1/charlescampbell-me";

config.commitUrlFormat = `${host}/charlescampbell.me/commit/{{hash}}`;

config.compareUrlFormat = `${host}/-/compare/{{previousTag}}...{{currentTag}}`;
