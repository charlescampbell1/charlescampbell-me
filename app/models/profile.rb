# frozen_string_literal: true

class Profile < ApplicationRecord
  validates_presence_of :title, :value
end
