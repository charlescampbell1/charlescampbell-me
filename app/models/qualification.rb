# frozen_string_literal: true

class Qualification < ApplicationRecord
  has_many :academic_year

  has_and_belongs_to_many :years

  validates_presence_of :title, :organisation
end
