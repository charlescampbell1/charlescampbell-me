# frozen_string_literal: true

class Unit < ApplicationRecord
  has_and_belongs_to_many :years
  has_and_belongs_to_many :tags

  validates_presence_of :unit, :grade, :description
end
