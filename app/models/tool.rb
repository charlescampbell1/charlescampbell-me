# frozen_string_literal: true

class Tool < ApplicationRecord
  validates_presence_of :tool, :colour
end
