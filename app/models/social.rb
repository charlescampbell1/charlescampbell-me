# frozen_string_literal: true

class Social < ApplicationRecord
  validates_presence_of :friendly_name, :url, :icon
end
