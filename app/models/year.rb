# frozen_string_literal: true

class Year < ApplicationRecord
  has_and_belongs_to_many :qualifications
  has_and_belongs_to_many :units

  validates_presence_of :title, :grade
end
