# frozen_string_literal: true

module StyleHelper
  def title(heading:, subheading:)
    render 'components/badge_title', heading: heading, subheading: subheading
  end
end
