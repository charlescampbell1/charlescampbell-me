# frozen_string_literal: true

module BadgeHelper
  def linked_badge_for(tool)
    link_to tool.tool,
            delete_tool_path(tool),
            class: "tool tool-#{tool.colour}",
            method: :delete,
            data: { confirm: "Are you sure you want to delete #{tool.tool}?" }
  end

  def linked_tag_for(tag)
    link_to tag.tag,
            delete_tag_path(tag),
            class: "tool tool-#{tag.colour}",
            method: :delete,
            data: { confirm: "Are you sure you want to delete #{tag.tag}?" }
  end

  def badge_for(tool)
    "<span class='tool tool-#{tool.colour}'>#{tool.tool}</span>".html_safe
  end

  def tag_for(tag)
    "<span class='tool tool-#{tag.colour}'>#{tag.tag}</span>".html_safe
  end
end
