# frozen_string_literal: true

module LinkHelper
  def active_link?(page)
    return 'active' if request.original_url.to_s.include?(page)

    ''
  end

  def active_home?
    return 'active' if request.original_url.to_s == root_url

    ''
  end
end
