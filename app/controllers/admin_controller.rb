# frozen_string_literal: true

class AdminController < ApplicationController
  layout 'backend'

  before_action :set_profile, only: %i[edit_profile update_profile]
  before_action :set_social, only: %i[edit_social update_social destroy_social]

  http_basic_authenticate_with name: ENV['BACKEND_USERNAME'], password: ENV['BACKEND_PASSWORD'], only: %i[backend]

  def new_profile
    @profile = Profile.new

    render 'backend/admin/profile/new'
  end

  def edit_profile
    render 'backend/admin/profile/edit'
  end

  def create_profile
    @profile = Profile.new(profile_params)

    if @profile.save
      redirect_to backend_path, notice: 'Profile has been created.'
    else
      redirect_to backend_path, notice: 'Failed to create profile.'
    end
  end

  def update_profile
    if @profile.update(profile_params)
      redirect_to backend_path, notice: 'Profile has been updated.'
    else
      redirect_to backend_path, notice: 'Failed to update profile.'
    end
  end

  def new_social
    @social = Social.new

    render 'backend/admin/social/new'
  end

  def create_social
    @social = Social.new(social_params)

    if @social.save
      redirect_to backend_path, notice: 'The social link has been created.'
    else
      redirect_to backend_path, notice: 'Failed to create the social link.'
    end
  end

  def edit_social
    render 'backend/admin/social/edit'
  end

  def update_social
    if @social.update(social_params)
      redirect_to backend_path, notice: 'The social link has been updated.'
    else
      redirect_to backend_path, notice: 'Failed to update the social link.'
    end
  end

  def destroy_social
    @social.destroy

    redirect_to backend_path, notice: 'The social link has been created.'
  end

  private

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def profile_params
    params.require(:profile).permit(:title, :value)
  end

  def set_social
    @social = Social.find(params[:id])
  end

  def social_params
    params.require(:social).permit(:friendly_name, :url, :icon, :display, :position)
  end
end
