# frozen_string_literal: true

class EducationController < ApplicationController
  layout 'backend', except: %i[index]

  before_action :set_qualifications, only: %i[index backend]
  before_action :set_qualification,
                only: %i[edit_qualification update_qualification qualification_details destroy_qualification]
  before_action :set_year, only: %i[edit_year update_year destroy_year]
  before_action :set_unit, only: %i[edit_unit update_unit destroy_unit]

  http_basic_authenticate_with name: ENV['BACKEND_USERNAME'], password: ENV['BACKEND_PASSWORD'], except: %i[index]

  def index
    render 'frontend/education/index'
  end

  def backend
    render 'backend/education/index'
  end

  def qualification_details
    render 'backend/education/qualification/details'
  end

  def new_qualification
    @qualification = Qualification.new

    render 'backend/education/qualification/new'
  end

  def create_qualification
    @qualification = Qualification.new(qualification_params)

    if @qualification.save
      redirect_to qualification_details_path(@qualification), notice: 'Qualification has been created.'
    else
      redirect_to backend_education_path, notice: 'Qualification failed to create.'
    end
  end

  def edit_qualification
    render 'backend/education/qualification/edit'
  end

  def update_qualification
    if @qualification.update(qualification_params)
      redirect_to qualification_details_path(@qualification), notice: 'Qualification has been updated.'
    else
      redirect_to qualification_details_path(@qualification), notice: 'Qualification failed to update.'
    end
  end

  def destroy_qualification
    @qualification.destroy

    redirect_to backend_education_path, notice: 'Qualification has been deleted.'
  end

  def new_year
    @year = Year.new

    render 'backend/education/year/new'
  end

  def create_year
    @qualification = Qualification.find(params[:qualification])
    @year = Year.new(year_params)

    respond_to do |format|
      if @year.save
        @qualification.years << @year
        format.html { redirect_to qualification_details_path(@qualification), notice: 'The year has been created.' }
      else
        format.html { redirect_to qualification_details_path(@qualification), notice: 'Failed to create year.' }
      end
    end
  end

  def edit_year
    render 'backend/education/year/edit'
  end

  def update_year
    @qualification = @year.qualifications.first

    if @year.update(year_params)
      redirect_to qualification_details_path(@qualification), notice: 'The year has been updated.'
    else
      redirect_to qualification_details_path(@qualification), notice: 'Failed to update year.'
    end
  end

  def destroy_year
    @qualification = @year.qualifications.first
    @year.destroy

    redirect_to qualification_details_path(@qualification), notice: 'The year has been deleted.'
  end

  def new_unit
    @unit = Unit.new

    render 'backend/education/unit/new'
  end

  def create_unit
    @year = Year.find(params[:year])
    @unit = Unit.new(unit_params)
    @qualification = @year.qualifications.first

    if @unit.save
      @year.units << @unit
      redirect_to qualification_details_path(@qualification), notice: 'Unit created.'
    else
      redirect_to qualification_details_path(@qualification), notice: 'Unit failed to create.'
    end
  end

  def edit_unit
    render 'backend/education/unit/edit'
  end

  def update_unit
    @qualification = @unit.years.first.qualifications.first

    if @unit.update(unit_params)
      redirect_to qualification_details_path(@qualification), notice: 'Unit Updated.'
    else
      redirect_to qualification_details_path(@qualification), notice: 'Unit failed to update.'
    end
  end

  def destroy_unit
    @qualification = @unit.years.first.qualifications.first

    @unit.destroy

    redirect_to qualification_details_path(@qualification), notice: 'Unit deleted.'
  end

  private

  def set_qualifications
    @qualifications = Qualification.all
  end

  def set_qualification
    @qualification = Qualification.find(params[:id])
  end

  def qualification_params
    params.require(:qualification).permit(:title, :organisation, :start_date, :end_date, :description, :url)
  end

  def set_year
    @year = Year.find(params[:id])
  end

  def year_params
    params.require(:year).permit(:title, :grade)
  end

  def set_unit
    @unit = Unit.find(params[:id])
  end

  def unit_params
    params.require(:unit).permit(:unit, :description, :grade)
  end
end
