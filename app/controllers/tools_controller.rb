# frozen_string_literal: true

class ToolsController < ApplicationController
  layout 'backend'

  before_action :set_tool, only: %i[destroy]

  http_basic_authenticate_with name: ENV['BACKEND_USERNAME'], password: ENV['BACKEND_PASSWORD'], except: %i[index]

  def new
    @tool = Tool.new

    render 'backend/tools/new'
  end

  def create
    @tool = Tool.new(tool_params)
    @tool.tool = @tool.tool.upcase

    if @tool.save
      redirect_to backend_path, notice: 'Tool created.'
    else
      redirect_to backend_path, notice: 'Failed to create tool.'
    end
  end

  def destroy
    @tool.destroy

    redirect_to backend_path, notice: 'Tool deleted.'
  end

  private

  def set_tool
    @tool = Tool.find(params[:id])
  end

  def tool_params
    params.require(:tool).permit(:tool, :colour, :status, :icon_url)
  end
end
