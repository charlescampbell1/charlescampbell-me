# frozen_string_literal: true

class TagController < ApplicationController
  layout 'backend'

  http_basic_authenticate_with name: ENV['BACKEND_USERNAME'], password: ENV['BACKEND_PASSWORD']

  def new
    @tag = Tag.new

    render 'backend/education/tag/new'
  end

  # rubocop:disable Metrics/AbcSize
  def create
    @unit = Unit.find(params[:unit])
    @qualification = @unit.years.first.qualifications.first

    @tag = Tag.new(tag_params)
    @tag.tag = @tag.tag.upcase

    if @tag.save
      @unit.tags << @tag
      redirect_to qualification_details_path(@qualification), notice: 'The tag has been created.'
    else
      redirect_to qualification_details_path(@qualification), notice: 'The tag failed to create.'
    end
  end
  # rubocop:enable Metrics/AbcSize

  def destroy
    @tag = Tag.find(params[:id])
    @qualification = @tag.units.first.years.first.qualifications.first

    @tag.destroy

    redirect_to qualification_details_path(@qualification), notice: 'The tag has been removed.'
  end

  private

  def tag_params
    params.require(:tag).permit(:tag, :colour)
  end
end
