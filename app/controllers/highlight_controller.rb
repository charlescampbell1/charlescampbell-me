# frozen_string_literal: true

class HighlightController < ApplicationController
  layout 'backend', except: %i[index]

  before_action :set_highlight, only: %i[show edit update destroy]
  before_action :set_highlights, only: %i[index backend]
  before_action :set_tools, only: %i[index backend]

  http_basic_authenticate_with name: ENV['BACKEND_USERNAME'], password: ENV['BACKEND_PASSWORD'], except: %i[index]

  def index
    render 'frontend/home/index'
  end

  def backend
    @profile = Profile.all
    @socials = Social.all.order('position ASC')

    render 'backend/home/index'
  end

  def new
    @highlight = Highlight.new

    render 'backend/highlights/new'
  end

  def edit
    render 'backend/highlights/edit'
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @highlight = Highlight.new(highlight_params)

    respond_to do |format|
      if @highlight.save
        format.html { redirect_to backend_path, notice: 'highlight created' }
      else
        format.html { redirect_to backend_path, notice: 'failed to create' }
      end
    end
  end

  def update
    respond_to do |format|
      if @highlight.update(highlight_params)
        format.html { redirect_to backend_path, notice: 'highlight updated' }
      else
        format.html { redirect_to backend_path, notice: 'failed to update' }
      end
    end
  end

  def destroy
    @highlight.destroy
    respond_to do |format|
      format.html do
        redirect_to backend_path, notice: 'highlight removed'
      end
    end
  end

  private

  def set_highlights
    @highlights = Highlight.order('start_date DESC')
  end

  def set_highlight
    @highlight = Highlight.find(params[:id])
  end

  def set_tools
    @tools = Tool.all
  end

  def highlight_params
    params.require(:highlight).permit(:title, :organisation, :start_date, :end_date, :description, :url)
  end
end
