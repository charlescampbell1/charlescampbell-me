class AddBlogToProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :blog, :string
  end
end
