class CreateJoinTableTagsUnits < ActiveRecord::Migration[6.0]
  def change
    create_join_table :tags, :units do |t|
      t.index %i[tag_id unit_id]
      t.index %i[unit_id tag_id]
    end
  end
end
