class CreateJoinTableQualificationsYears < ActiveRecord::Migration[6.0]
  def change
    create_join_table :qualifications, :years do |t|
      t.index %i[qualification_id year_id]
      t.index %i[year_id qualification_id]
    end
  end
end
