# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_22_110507) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "highlights", force: :cascade do |t|
    t.string "title"
    t.string "organisation"
    t.date "start_date"
    t.date "end_date"
    t.string "description"
    t.string "url"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "title"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "friendly_name"
    t.string "url"
    t.string "position"
    t.string "blog"
  end

  create_table "qualifications", force: :cascade do |t|
    t.string "title"
    t.string "organisation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "qualifications_years", id: false, force: :cascade do |t|
    t.bigint "qualification_id", null: false
    t.bigint "year_id", null: false
    t.index ["qualification_id", "year_id"], name: "index_qualifications_years_on_qualification_id_and_year_id"
    t.index ["year_id", "qualification_id"], name: "index_qualifications_years_on_year_id_and_qualification_id"
  end

  create_table "socials", force: :cascade do |t|
    t.string "friendly_name"
    t.string "url"
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "display"
    t.string "position"
  end

  create_table "tags", force: :cascade do |t|
    t.string "tag"
    t.string "colour"
    t.bigint "education_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["education_id"], name: "index_tags_on_education_id"
  end

  create_table "tags_units", id: false, force: :cascade do |t|
    t.bigint "tag_id", null: false
    t.bigint "unit_id", null: false
    t.index ["tag_id", "unit_id"], name: "index_tags_units_on_tag_id_and_unit_id"
    t.index ["unit_id", "tag_id"], name: "index_tags_units_on_unit_id_and_tag_id"
  end

  create_table "tools", force: :cascade do |t|
    t.string "tool"
    t.string "colour"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "units", force: :cascade do |t|
    t.string "unit"
    t.string "grade"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "units_years", id: false, force: :cascade do |t|
    t.bigint "unit_id", null: false
    t.bigint "year_id", null: false
    t.index ["unit_id", "year_id"], name: "index_units_years_on_unit_id_and_year_id"
    t.index ["year_id", "unit_id"], name: "index_units_years_on_year_id_and_unit_id"
  end

  create_table "years", force: :cascade do |t|
    t.string "title"
    t.string "grade"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
